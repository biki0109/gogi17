package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi17/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi17Client struct {
	pb.Gogi17Client
}

func NewGogi17Client(address string) *Gogi17Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi17 service", l.Error(err))
	}

	c := pb.NewGogi17Client(conn)

	return &Gogi17Client{c}
}
